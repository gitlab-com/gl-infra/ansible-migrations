---
- name: Checks
  hosts: localhost
  gather_facts: false

  tasks:
    - assert:
        that:
          - "migration_env in ['gstg', 'gprd']"
          - "ops_api_token|length > 0"

- name: Prepare Patroni hosts
  hosts: "{{ migration_env }}-patroni"
  gather_facts: false

  tasks:
    - name: Stop chef-client
      service:
        name: chef-client
        state: stopped
      become: true

    - name: Pause the Patroni cluster
      command: gitlab-patronictl pause
      ignore_errors: true

    - name: Verify we're in maintenance mode
      command: gitlab-patronictl list
      register: patronictl
      failed_when: "'Maintenance mode' not in patronictl.stdout"

    # We stop Patroni because when we restart Consul (see why below)
    # it makes Patroni snap out of maintenance mode (somehow), so we
    # take it out of the picture for the time being.
    # Stopping Patroni while the cluster is paused doesn't affect
    # PostgreSQL.
    - name: Stop Patroni
      service:
        name: patroni
        state: stopped
      become: true

- name: Apply Patroni Chef changes
  hosts: localhost
  gather_facts: false

  tasks:
    - name: Apply Patroni Chef changes
      merge_and_apply:
        mr_iid: "{{ merge_requests[migration_env]['patroni'] }}"
        api_token: "{{ ops_api_token }}"
        is_production: "{{ is_production }}"

- name: Rolling out changes to Patroni
  hosts: "{{ migration_env }}-patroni"
  gather_facts: true
  serial:
    - 1
    - 5

  tasks:
    - name: Converge Chef
      command: chef-client
      become: true

    # Chef will reload Consul but it is not enough as we're enabling `enable_script_checks`
    # and it seems that it needs a hard restart for it to take effect.
    - name: Restart Consul
      service:
        name: consul
        state: restarted
      become: true

    - name: Start Patroni
      service:
        name: patroni
        state: started
      become: true

- name: Restore Patroni hosts state
  hosts: "{{ migration_env }}-patroni"
  gather_facts: false

  tasks:
    - name: Start chef-client
      service:
        name: chef-client
        state: started
      become: true

    - name: Resume the Patroni cluster
      command: gitlab-patronictl resume
      ignore_errors: true

  post_tasks:
    - name: Verify we're not in maintenance mode
      command: gitlab-patronictl list
      register: patronictl
      failed_when: "'Maintenance mode' in patronictl.stdout"

    - name: Verify presence of a DNS record
      command: dig @localhost -p 8600 +short master.patroni.service.consul. replica.patroni.service.consul.
      register: dig
      failed_when: "ansible_default_ipv4.address not in dig.stdout_lines"

- name: Apply clients Chef changes
  hosts: localhost
  gather_facts: false

  tasks:
    - name: Apply clients Chef changes
      merge_and_apply:
        mr_iid: "{{ merge_requests[migration_env]['clients'] }}"
        api_token: "{{ ops_api_token }}"
        is_production: "{{ is_production }}"

- name: Rolling out changes to clients
  hosts: "{{ migration_env }}-all"
  gather_facts: false
  become: true
  serial: "30%"

  tasks:
    - name: Converge Chef
      command: chef-client

- name: Verify clients
  hosts: "{{ migration_env }}-worker"
  gather_facts: false
  become: true

  tasks:
    - name: Verify presence of a DNS record
      command: gitlab-rails r 'puts Gitlab::Database::LoadBalancing.proxy.load_balancer.instance_eval { @host_list }.hosts.map(&:host).count'
      register: rails_runner
      failed_when: "rails_runner.stdout|int != 5"
