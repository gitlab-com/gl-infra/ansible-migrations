# Maintenance Mode

This is a playbook for putting an environment in a maintenance mode,
namely blocking all incoming traffic and shutting down services that
process such traffic (Puma, Sidekiq, ...).

## How to run

First, setup the console server for Kubernetes operations, as described [here][k8s-oncall].

**Do the setup for the regional cluster and the 3 zonal clusters! Make sure that
switching the kubectl context is working like this:**

```
kubectl config use-context gke_gitlab-staging-1_us-east1_gstg-gitlab-gke
kubectl config use-context gke_gitlab-staging-1_us-east1-d_gstg-us-east1-d
...
```

Then run the following:

```
workstation $ eval `ssh-agent -s`
workstation $ ssh-add ~/.ssh/id_rsa
workstation $ ssh -A bastion-01-inf-gstg.c.gitlab-staging-1.internal
bastion     $ ssh -A console-01-sv-gstg.c.gitlab-staging-1.internal
console     $ pip install python-consul ansible
console     $ git clone https://gitlab.com/gitlab-com/gl-infra/ansible-migrations.git
console     $ cd ansible-migrations/maintenance-mode
# Change variables as needed in variables.yml
console     $ export CLOUDFLARE_API_TOKEN=xyz # Find value in 1Password
console     $ export CLOUDFLARE_ZONE=xyz # e.g. gitlab.com or staging.gitlab.com
console     $ export ENVIRONMENT=xyz # e.g. gstg or gprd
console     $ export GCP_PROJECT=xyz # e.g. gitlab-staging-1 or gitlab-production
# To start maintenance mode run:
console     $ ~/.local/bin/ansible-playbook -e @variables.yml -t step-1,step-2,step-3 enable.yml
# When ready, shutdown sidekiq:
console     $ ~/.local/bin/ansible-playbook -e @variables.yml -t step-4 enable.yml
# To stop maintenance mode run:
console     $ ~/.local/bin/ansible-playbook -e @variables.yml -t start-services disable.yml
# When ready, open up access to the world:
console     $ ~/.local/bin/ansible-playbook -e @variables.yml -t open-access disable.yml
```

[k8s-oncall]: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md#accessing-clusters-via-console-servers
