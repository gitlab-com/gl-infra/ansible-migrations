# gl-infra-9885-upgrade-patroni

Upgrade the python pypi package version for patroni.

Note: The task "Ensure replicas are ready to serve queries" is specifically
configure to not fail when it never receives a return code of 0 for its
dig check. This is because dig will always fail when a cluster member is not
configured for failover or load-balancing.  This is a temporary stop-gap to
prevent premature play termination, despite a normal status. An issue has
been created to track this issue:

https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/12719

## Execution

Initialize the pipenv shell:

```bash
cd ~/workspace/ansible-migrations
tmux
export OPS_API_TOKEN='changeme'
export MIGRATION_ENV='gstg'
export ANSIBLE_LIBRARY="${HOME}/workspace/ansible-migrations/modules"
pipenv --python $(which python3) shell
PIPENV_VERBOSITY=-1 pipenv sync
```

Make sure the inventory files are reflecting reality!

```bash
ansible-inventory --inventory-file=gl-infra-9885-upgrade-patroni/gstg.yaml --list | jq "[.secondary.hosts, .primary.hosts]"
```

Compare to:

```bash
ssh $(ansible-inventory --inventory-file=gl-infra-9885-upgrade-patroni/gstg.yaml --list | jq -r '.primary.hosts[]') 'sudo gitlab-patronictl list'
```

Usage:

```bash
# ansible-playbook --inventory-file=gl-infra-9885-upgrade-patroni/gstg.yaml [--step] [--check] gl-infra-9885-upgrade-patroni/playbook.yaml
```

Confirm the targets:

```bash
ansible-inventory --inventory-file=gl-infra-9885-upgrade-patroni/gstg.yaml --list | jq
```

Check syntax of playbook:

```bash
ansible-playbook --inventory-file=gl-infra-9885-upgrade-patroni/gstg.yaml --extra-vars=@gl-infra-9885-upgrade-patroni/variables.yaml --list-tasks gl-infra-9885-upgrade-patroni/playbook.yaml
```

Example "dry-run" (check) invocation:

```bash
ANSIBLE_LOG_PATH=log/playbook_gl-infra-9885-upgrade-patroni_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --inventory-file=gl-infra-9885-upgrade-patroni/gstg.yaml --extra-vars=@gl-infra-9885-upgrade-patroni/variables.yaml --check gl-infra-9885-upgrade-patroni/playbook.yaml
```

Example invocation:

```bash
ANSIBLE_LOG_PATH=log/playbook_gl-infra-9885-upgrade-patroni_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --inventory-file=gl-infra-9885-upgrade-patroni/gstg.yaml --extra-vars=@gl-infra-9885-upgrade-patroni/variables.yaml --step gl-infra-9885-upgrade-patroni/playbook.yaml
```

To execute or resume playbook execution from a specified task:

```bash
ANSIBLE_LOG_PATH=log/playbook_gl-infra-9885-upgrade-patroni_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --start-at-task="Verify that the Patroni Service is inactive" --step --inventory-file=gl-infra-9885-upgrade-patroni/gstg.yaml --extra-vars=@gl-infra-9885-upgrade-patroni/variables.yaml --limit=patroni-07-db-gstg.c.gitlab-staging-1.internal gl-infra-9885-upgrade-patroni/playbook.yaml
```

