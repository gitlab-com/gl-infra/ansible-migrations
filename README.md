# Ansible Migrations

TODO: Add project summary.

## Description

TODO: Add project description.

## Setup

Invoke the following setup commands on a console server.

### Clone db-ops repo

```bash
cd ~
mkdir -p workspace
cd workspace
git clone git@gitlab.com:gitlab-com/gl-infra/ansible-migrations.git
cd ansible-migrations
```

### Install Python 3.7

#### GNU Linux only

Install Python 3.7.9 and pipenv.

```bash
git clone https://github.com/asdf-vm/asdf.git ~/.asdf
cd ~/.asdf
git checkout "$(git describe --abbrev=0 --tags)"
source $HOME/.asdf/asdf.sh # Also add to ~/.bash_profile
source $HOME/.asdf/completions/asdf.bash # Also add to ~/.bash_profile
asdf plugin add python
asdf install python 3.7.9
python3 -m pip install --upgrade pip
python3 -m pip install --user pipenv
```

## Invocation instructions

See the `README.md` file in each per-issue subdirectory for instructions on
how to invoke each ansible instruction set.