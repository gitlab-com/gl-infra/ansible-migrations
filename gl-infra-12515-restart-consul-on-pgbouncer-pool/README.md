# gl-infra-12515

Restart Consul agent on the PgBouncer pool. PgBouncer depends on Consul to be
able to lookup the current Patroni leader. To avoid any potential errors while
the agent is restarted, we add a static DNS entry for the current Patroni leader
that PgBouner can lookup (via dnsmasq).

## Usage

```bash
TARGET_ENV=<gstg_or_gprd> ansible-playbook -i inventory.txt -v playbook.yml
```
