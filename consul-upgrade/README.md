# Consul Upgrade

This is a playbook for upgrading Consul cluster and its agents.

## How to run

Create merge requests that bump the consul version (see examples [here][example-1] and [here][example-2]),
fill in their IDs under the target environment in `variables.yml`, change any other variables
as needed (e.g. `consul_version`) in `variables.yml`, then run the following:

```
workstation $ eval `ssh-agent -s`
workstation $ ssh-add ~/.ssh/id_rsa
workstation $ ssh -A bastion-01-inf-gstg.c.gitlab-staging-1.internal
bastion     $ pip install python-consul ansible
bastion     $ git clone https://gitlab.com/gitlab-com/gl-infra/ansible-migrations.git
bastion     $ cd ansible-migrations/consul-upgrade
bastion     $ tmux
bastion     $ vim variables.yml # Change variables as needed in variables.yml
bastion     $ export ANSIBLE_HOST_KEY_CHECKING=False
bastion     $ export OPS_API_TOKEN=xyz # Generate one from ops.gitlab.net
bastion     $ export MIGRATION_ENV=gstg # E.g. gstg or gprd
bastion     $ ~/.local/bin/ansible-playbook -M ../modules/ -i ../consul-inventory/consul_io.py -e @variables.yml --step -f 2 playbook.yml
```

[example-1]: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/-/merge_requests/3102
[example-2]: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/-/merge_requests/3103
